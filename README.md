# Roszas Offspring

A puzzle platformer written entirely in MicroBlocks for GnuBlocks (MicroBlocks Linux VM). Play as Rosza the rabbit, and bring carrots to your offspring taking care of not falling from too high!

![A screenshot of the game portraying Rosza the rabbit carrying a carrot](screenshot.png)

# Story

You are Rosza the rabbit, and you need to get carrots to your hungry offspring in increasingly complex lair configurations.

When not carrying any carrots, you can jump 2 tiles high and fall from 3 tiles high without dying. When carrying a carrot, you can only jump 1 tile high, and falling from higher than 2 tiles will kill you.

# Running It

Run `./run` to start it :)

# Playing

Use the `←` and `→` keys to walk. Use `↑`to jump. When carrying a carrot, drop it with `↓`.

# Sources

The sources for the MicroBlocks VM binary (`run` file) can be found in the [MicroBlocks repository](bitbucket.org/john_maloney/smallvm/).

The sources for the game and level editor are in the `.ubp` files, and can be read with the MicroBlocks IDE, which you can get at [microblocks.fun](https://microblocks.fun).
